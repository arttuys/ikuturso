defmodule BloomTest do
  use ExUnit.Case
  require Logger

  test "HIBP database works appropriately for simple, known compromised passwords hashed" do
    binary_file =
      File.read!(
        Path.join(:code.priv_dir(:ikuturso), Path.join(["datasets", "hibp-top-1-percent.iktb"]))
      )

    filter = IkuTurso.Tools.BloomFilter.from_binary(binary_file)

    assert Enum.all?(
             ["1234", "root", "password", "login", "letmein", "123456", "qwerty"]
             |> Enum.map(fn x -> :crypto.hash(:sha, x) |> Base.encode16() end),
             fn x ->
               IkuTurso.Tools.BloomFilter.check(filter, x) &&
                 IkuTurso.Tools.BloomFilter.check_from_binary(binary_file, x)
             end
           ),
           "compromised passwords are not appropriately detected"
  end
end
