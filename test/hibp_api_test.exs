defmodule HIBPAPITest do
  use ExUnit.Case
  require Logger

  test "returns a fail on a known compromise" do
    known_bad_passwords = ["root", "hunter2", "toooor", "1234"]

    Enum.each(known_bad_passwords, fn pwd ->
      Logger.info("Testing '#{pwd}'")
      :fail = IkuTurso.ExternalConnections.HIBP.check_password_compromise(pwd)
    end)
  end

  test "behaves well asynchronously" do
    known_bad_passwords = ["password", "letmein", "qwerty", "asdfgh"]

    res =
      Task.async_stream(known_bad_passwords, fn pwd ->
        IkuTurso.ExternalConnections.HIBP.check_password_compromise(pwd)
      end)
      |> Enum.map(fn {:ok, a} -> a end)
      |> Enum.all?(fn x -> x == :fail end)

    assert res, "all passwords didnt fail gracefully"
  end
end
