defmodule DAFSATest do
  use ExUnit.Case
  require Logger

  test "fuzzing does not crash a DAFSA, and properties are upheld" do
    1..5
    |> Enum.each(fn time ->
      Logger.info("Test run #{Integer.to_string(time)}/5")

      # This is very important to test; there are some specified preconditions in which a DAFSA may misbehave
      # However, any misbehavior should NOT result in an infinite loop or a crash - at worst, bad scoring should result.

      # Define some known problematic combinations
      known_problematic = [
        <<0>>,
        <<9, 231, 2, 0, 11, 65, 0>>,
        <<75, 31, 99, 33, 0, 11, 56>>,
        <<99, 11, 35, 77, 0>>,
        <<0, 0, 0, 0, 0, 0>>,
        <<0, 0>>,
        <<0, 0>>,
        "",
        "a",
        "b",
        "g",
        "d",
        "al"
      ]

      # Initialize a stream of random bytes, ensuring that we do not accidentally result in generating bad combinations
      crypto_bytes =
        Stream.repeatedly(fn -> :crypto.strong_rand_bytes(Enum.random(1..256)) end)
        |> Enum.take(80 - length(known_problematic))
        |> Enum.filter(fn x -> !(x in ["alpha", "beta", "gamma", "delta"]) end)

      # Intersperse these, shuffle, and make a list
      generated_bytes =
        Enum.concat([crypto_bytes, known_problematic]) |> Enum.shuffle() |> Enum.to_list()

      # Pull into into 4 sets
      set_a = generated_bytes |> Enum.slice(0, 20)
      set_b = generated_bytes |> Enum.slice(20, 20)
      set_c = generated_bytes |> Enum.slice(40, 20)
      set_d = generated_bytes |> Enum.slice(60, 20)
      # And finally, intersperse our test words
      final_list =
        Enum.concat([
          ["alpha"],
          set_a,
          ["beta"],
          set_b,
          ["gamma"],
          set_c,
          ["delta", "alpha"],
          set_d
        ])

      # Finally, generate a DAFSA
      dafsa = IkuTurso.Tools.DAFSASet.build_dafsa_set(final_list)

      # Test that assertions hold for the baseline case
      Enum.reduce(["alpha", "beta", "gamma", "delta"], -1, fn str, last_score ->
        matches = IkuTurso.Tools.DAFSASet.find_substrings(dafsa, str, true)

        Enum.each(matches, fn {found_str, 0, _score} ->
          assert(String.contains?(str, found_str), "substring does not match")
        end)

        # Retrieve appropriate structure
        {_, _, found_score} =
          Enum.find(matches, nil, fn {found_str, 0, _score} -> found_str == str end)

        # Assert appropriate scoring
        assert found_score > last_score, "scoring misordered"
        last_score
      end)

      Logger.info("Dictionary generation with fuzzing successful, next testing fuzzed queries")

      # This is equally important: we should test all sorts of random queries, to ensure that we do not crash even if we are given bad, faulty input

      [true, false]
      |> Enum.each(fn enable_wildcards ->
        [true, false]
        |> Enum.each(fn first_byte_match ->
          [true, false]
          |> Enum.each(fn allow_empty_match ->
            # Generate some known bad queries
            crypto_queries =
              Stream.repeatedly(fn -> :crypto.strong_rand_bytes(Enum.random(1..16)) end)
              |> Enum.take(50)

            known_problematic_queries = ["", "*alpha?", "??*", "beta?", <<0>>, <<0, "*"::utf8>>]

            problem_queries =
              Enum.concat([crypto_queries, known_problematic_queries]) |> Enum.shuffle()

            Logger.info(
              "Running #{length(problem_queries)} problem queries, #{
                if enable_wildcards, do: "allowed wildcards", else: "disabled wildcards"
              }, #{if first_byte_match, do: "first letter must match", else: "all submatches"}, #{
                if allow_empty_match, do: "empty matches OK", else: "no empty matches"
              }"
            )

            problem_queries
            |> Enum.each(fn query ->
              matches_found =
                IkuTurso.Tools.DAFSASet.find_substrings(
                  dafsa,
                  query,
                  first_byte_match,
                  allow_empty_match,
                  enable_wildcards
                )

              # We know there is an empty string, as we ingested one to the dictionary. However, we shouldn't find one if we have disabled em
              assert(
                Enum.any?(matches_found, fn {str, _, _} -> str == "" end) == allow_empty_match,
                "no empty match found when expected"
              )
            end)
          end)
        end)
      end)
    end)
  end

  test "basic cases behave appropriately" do
    # Generate a simple dictionary
    dafsa =
      IkuTurso.Tools.DAFSASet.build_dafsa_set(["alpha", "alps", "alp", "beta"])

    test_cases = [
      {["alpha", true, false, false], [{"alpha", 0, 0}, {"alp", 0, 2}]},
      {["alphabeta", true, false, false], [{"alpha", 0, 0}, {"alp", 0, 2}]},
      {["alphabeta", false, false, false], [{"alpha", 0, 0}, {"alp", 0, 2}, {"beta", 5, 3}]},
      {["alp*", true, false, true], [{"alpha", 0, 0}, {"alp", 0, 2}, {"alps", 0, 1}]},
      {["alp?", true, false, true], [{"alp", 0, 2}, {"alps", 0, 1}]},
      {["alp*", true, false, false], [{"alp", 0, 2}]},
      {["alp?", true, false, false], [{"alp", 0, 2}]}
    ]

    Enum.each(test_cases, fn {params, e_results} ->
      f_result = MapSet.new(apply(IkuTurso.Tools.DAFSASet, :find_substrings, [dafsa | params]))

      if !MapSet.equal?(MapSet.new(e_results), f_result) do
        IO.inspect(e_results)
        IO.inspect(f_result)

        flunk("expected a certain result, but got something else")
      end
    end)
  end
end
