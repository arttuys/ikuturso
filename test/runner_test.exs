defmodule RunnerTest do
  use ExUnit.Case

  test "basic characteristics behave appropriately" do
    # Generate strategy
    # 5 min, 12 max, no strength limit
    strategy = IkuTurso.Strategies.BasicCharacteristics.generate_strategy(5, 12)
    {:conflict, {:too_long, 12}} = IkuTurso.Runner.run_strategy([strategy], "1234567890123456")
    {:conflict, {:too_short, 5}} = IkuTurso.Runner.run_strategy([strategy], "123")
    {:conflict, {:insufficient_catgs, 3}} = IkuTurso.Runner.run_strategy([strategy], "1234567")
    {:conflict, {:insufficient_catgs, 3}} = IkuTurso.Runner.run_strategy([strategy], "1234a67")
    {:conflict, {:insufficient_catgs, 3}} = IkuTurso.Runner.run_strategy([strategy], "123A567")
    {:success} = IkuTurso.Runner.run_strategy([strategy], "123%56a")
  end
end
