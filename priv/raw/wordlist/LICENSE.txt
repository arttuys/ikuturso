English word frequency dataset is licensed under, CC BY-SA 3.0, by Hermit Dave, as sourced from: https://github.com/hermitdave/FrequencyWords

Per CC Share-Alike, the resulting transformed DAFSA dataset is also CC BY-SA 3.0