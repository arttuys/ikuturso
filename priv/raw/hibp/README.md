# HIBP Bloom Filter

This file describes the rebuild process for the HIBP bloom filter included with this package, matching to over 5 million hashes of known compromised passwords - or roughly top one percent of all compromised passwords known to HIBP. 

## Source material and licensing

This work is a derivative work based on the Pwned Passwords dataset, as found from [Have I Been Pwned](https://www.troyhunt.com/introducing-306-million-freely-downloadable-pwned-passwords/), created by Troy Hunt.

As specified on the webpage above, no explicit requirement for attribution exists, but it will be highly appreciated. Same applies for this derivative work - no attribution is required, but will certainly be appreciated.

## Building the filter

1. Download the dataset from the link above, _making sure_ to select the SHA1 version ordered by prevalence. Hash-ordered version will result in a valid filter, but its impact will be much lower with essentially randomly selected passwords, instead of known bad, common ones.
2. Extract the file to `priv/raw/hibp`
3. Go to the root directory of the project, and run `mix build_datasets`
4. When the command completes, you will find the files from `priv/datasets`