defmodule IkuTurso do
  @moduledoc """
  Documentation for IkuTurso.
  """

  @doc """
  Hello world.

  ## Examples

      iex> IkuTurso.hello()
      :world

  """
  @spec hello() :: :ok
  def hello do
    :world

    IkuTurso.ExternalConnections.HIBP.check_password_compromise("magic")
  end
end
