defmodule Mix.Tasks.BuildDatasets do
  use Mix.Task
  require Logger

  @project_root Path.join(File.cwd!(), "priv")
  @hibp_pwned_pwds_name "pwned-passwords-sha1-ordered-by-count-v4.txt"

  defp build_hibp_popular do
    # Using bloom filters, it is a viable proposition to store a subset of HIBP data. That way, most common bad passwords can be found quickly locally, without having to resort to API calls
    # Assuming that we will accept a max of 20 megabytes for filter size and a false positive rate of 1/1 000 000, we can approximate the appropriate parameters with https://hur.st/bloomfilter/
    #
    # With this knowledge, it can be approximated that we should store at max 5,834,504 items, to maintain a false positive rate of one in a million. We should use 20 hashes per item for that.
    #
    # The source files will not be supplied with this package due to their large size. However, instructions for repeating the build will be provided.

    Logger.info(
      "Building HIBP top 1% bloom filter, allowing one in a million false positive rate"
    )

    # Initialize a raw structure

    in_path = Path.join(@project_root, Path.join(["raw", "hibp", @hibp_pwned_pwds_name]))

    if File.exists?(in_path) do
      base_filter = IkuTurso.Tools.BloomFilter.raw_bloom_filter(20 * 1024 * 1024 * 8, 20)
      out_path = Path.join(@project_root, Path.join(["datasets", "hibp-top-1-percent.iktb"]))
      # Open a stream

      stream = File.stream!(in_path)

      composed_filter =
        Enum.reduce_while(stream, {0, base_filter}, fn line, {count, filter} ->
          normalized_line = String.trim(line) |> String.split(":") |> Enum.at(0)
          count_now = count + 1

          if count_now < 5_834_504 do
            {:cont, {count_now, IkuTurso.Tools.BloomFilter.mark(filter, normalized_line)}}
          else
            {:halt, IkuTurso.Tools.BloomFilter.mark(filter, normalized_line)}
          end
        end)

      File.write!(out_path, IkuTurso.Tools.BloomFilter.to_binary(composed_filter), [:write])
    else
      Logger.warn("No HIBP file found at '#{in_path}', skipping build")
    end
  end

  defp build_en50k do
    Logger.info("Building English 50k top words DAFSA set for dictionary usage")

    in_path = Path.join(@project_root, Path.join(["raw", "wordlist", "en_50k.txt"]))

    # A DAFSA can be used to store a compact, fast dictionary of common words in a sorted order

    if File.exists?(in_path) do
      out_path = Path.join(@project_root, Path.join(["datasets", "en-50k.iktdafsa"]))

      # Extract words. Note that we filter out all words less than 2 letters, as these seem to be mostly spurious
      file_lines = File.stream!(in_path) |> Enum.map(fn str -> [word, _] = String.split(str, " ", parts: 2); word |> String.trim() end) |> Enum.reject(fn str -> String.length(str) < 2 end)

      # Generate a DAFSA structure
      dafsa = IkuTurso.Tools.DAFSASet.build_dafsa_set(file_lines)

      # Save to file
      File.write(out_path, IkuTurso.Tools.DAFSASet.to_compressed_data(dafsa), [:write])
    else
      Logger.warn("No English 50k dictionary found at '#{in_path}}, skipping build'")
    end
  end

  @shortdoc "(Re)builds password bloom filters and trie dictionaries from source files"
  def run(_) do


    build_hibp_popular()
    build_en50k()
    Logger.info("Complete!")
  end
end
