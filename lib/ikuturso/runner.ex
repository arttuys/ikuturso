defmodule IkuTurso.Runner do
  defp run_strategy_internal([], _, worst_result), do: worst_result

  defp run_strategy_internal([head | tail], str, worst_result) do
    res = apply(head.module, :run_strategy, [head, str])

    case res do
      # These results result in an immediate stop
      {:error} ->
        {:error}

      {:conflict, err} ->
        {:conflict, err}

      # Warnings override if we don't already have one
      {:warning, warn} ->
        next_worst =
          if elem(worst_result, 0) == :warning, do: worst_result, else: {:warning, warn}

        run_strategy_internal(tail, str, next_worst)

      {:success} ->
        run_strategy_internal(tail, str, worst_result)

      _ ->
        raise "unexpected result from strategy runner"
    end
  end

  @doc """
    Runs a given list of strategies in the given order, reporting success or failure as specified in `IkuTurso.Strategies.Strategy`

    Do note that this runner does not parallelize its strategies in any way. This onus is on the caller, particularly considering that some strategies may use nontrivial amounts of memory, rendering duplication across processes a bad proposition.
"""
  def run_strategy(strategy_list, str) do
    try do
      run_strategy_internal(strategy_list, str, {:success})
    rescue
      # Failsafe catch, in case of an internal error of some nature
      RuntimeError -> {:error}
    end
  end
end
