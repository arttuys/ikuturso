defmodule IkuTurso.ExternalConnections.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: IkuTurso.Worker.start_link(arg)
      # {IkuTurso.Worker, arg},
      {IkuTurso.ExternalConnections.HIBP, nil},
      :hackney_pool.child_spec(:hibp_pool,
        timeout: Application.get_env(:ikuturso, :hibp_api_timeout_ms, 4000),
        max_connections: Application.get_env(:ikuturso, :hibp_connection_pool_size, 15)
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: IkuTurso.ExternalConnections.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
