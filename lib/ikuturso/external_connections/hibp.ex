defmodule IkuTurso.ExternalConnections.HIBP do
  use GenServer

  @doc """
  Initializes Have I Been Pwned checker.

  The operation of this checker is essentially controlled by environmental variables, with little user configuration required by default. Default options are designed for relatively modest loads and a reliable, fast connection.

  Essentially, requests are queued, admitted at certain intervals, and responded to asynchronously

  ## Environmental variables

  Following environmental variables under app `:ikuturso` can be used to control the behavior of this checker

  - `:hibp_maximum_queue_length`: no more than this many requests are allowed in the queue. If a request would exceed this limit, `:try_again_later` is returned instead
  - `:hibp_request_interval`: after each succesful request, how long it should take at least to admit the next one?
  - `:hibp_maximum_wait_time`: if the wait needed to admit the next request would be too long, the checker will return `:try_again_later` instead
  - `:hibp_api_timeout` : how long we are willing to wait for a network timeout?
  - `:hibp_error_interval_base`: after succeeding errors, the error waiting interval is geometrically grown. `:hibp_error_interval_base` defines the base waiting time for the first error, currently 1000ms
  """
  def init(_), do: {:ok, {0, 0}}

  @doc """
  Starts the Have I Been Pwned call API
  """
  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: IkuTurso.ExternalConnections.HIBP)
  end

  @doc """
    Checks a password for a compromise, using the K-anonymity protocol as provided by the Have I Been Pwned API

    This function has a set timeout at 1500ms beyond the maximum timeout permitted for the API call, plus a possible wait time for the next request. In case of such a timeout, `:error` is returned

    One of the possible values here are returned:
      - `:ok`: password was not found from the list, and is not known to HIBP
      - `:fail`: password was found from the list, and should be considered compromised
      - `:try_again_later`: password was unable to be evaluated due to rate-limiting either at our side or serverside, request should be attempted some time later
      - `:error`: password was not evaluated due to a server error (e.g blocked, unable to connect)
  """
  @spec check_password_compromise(String.t()) :: :ok | :fail | :try_again_later | :error
  def check_password_compromise(str) when is_bitstring(str) and byte_size(str) > 0 do
    try do
      # First, we need to check for queue length
      hibp_pid = Process.whereis(__MODULE__)
      max_queue_length = Application.get_env(:ikuturso, :hibp_maximum_queue_length, 10)

      hibp_error_state =
        if hibp_pid == nil do
          :error
        else
          case Process.info(hibp_pid, :message_queue_len) do
            # If our queue is at an unmanageable size, do not allow any further requests
            {_, len} when len > max_queue_length -> :try_again_later
            _ -> nil
          end
        end

      # If we found discrepancies from the process status, return that - otherwise call onward
      cond do
        hibp_error_state != nil ->
          hibp_error_state

        true ->
          # Before engaging with the HIBP remote server, hash our password
          {hash_prefix, hash_suffix} =
            :crypto.hash(:sha, str) |> Base.encode16() |> String.split_at(5)

          # Request a result from the GenServer. Set a fixed timeout, so we can provide an upper bound for our caller
          returned_hashes_or_err =
            GenServer.call(
              __MODULE__,
              {:lookup, hash_prefix},
              Application.get_env(:ikuturso, :hibp_api_timeout, 4000) +
                Application.get_env(:ikuturso, :hibp_maximum_wait_time, 500) + 1500
            )

          case returned_hashes_or_err do
            {:ok, hash_list} ->
              # We have a valid hash list. Check what we find
              if Enum.any?(hash_list, fn found_suffix -> hash_suffix == found_suffix end) do
                :fail
              else
                :ok
              end

            # Other results, return as is
            other ->
              other
          end
      end
    rescue
      # Failsafe stop at a runtime error
      RuntimeError -> :error
    end
  end

  def check_password_compromise(_) do
    :error
  end

  # Server callbacks

  def handle_cast({:successful_call}, {next_appropriate_call_ms, _error_count}) do
    delay = Application.get_env(:ikuturso, :hibp_request_interval, 50)

    {:noreply, {max(:os.system_time(:milli_seconds) + delay, next_appropriate_call_ms), 0}}
  end

  def handle_cast({:errored_call}, {next_appropriate_call_ms, error_count}) do
    maximum_error_count =
      min(Application.get_env(:ikuturso, :hibp_maximum_counted_errors, 5), error_count + 1)

    delay =
      Application.get_env(:ikuturso, :hibp_error_interval_base, 1000) *
        (:math.pow(2, error_count) |> round())

    {:noreply,
     {max(:os.system_time(:milli_seconds) + delay, next_appropriate_call_ms), maximum_error_count}}
  end

  @doc """
      Calls HIBP API endpoint, responding with a list of hash suffixes or an error to the caller.
      Normally, this function should not be called directly, but instead via `check_password_compromise/1`
  """
  def async_hibp_request(respond_to, prefix_hash) do
    # Do a request to HIBP API
    hibp_api_result =
      HTTPoison.get(
        "https://api.pwnedpasswords.com/range/#{prefix_hash}",
        [{"User-Agent", "IkuTurso-HIBP-Checker"}],
        timeout: Application.get_env(:ikuturso, :hibp_api_timeout, 4000),
        hackney: [pool: :hibp_pool]
      )

    case hibp_api_result do
      # Something failed. Throttle requests so that we have to wait the maximum wait time until the next request.
      {:error, _} ->
        GenServer.reply(respond_to, :error)
        GenServer.cast(__MODULE__, {:errored_call})

      {:ok, %HTTPoison.Response{status_code: status, body: body}} ->
        case status do
          # If we get a list of suffixes, return it as an Elixir list
          200 ->
            GenServer.reply(
              respond_to,
              {:ok,
               body
               |> String.split(["\n", "\r", "\r\n"])
               |> Enum.map(fn x -> String.split(x, ":", trim: true) |> Enum.fetch!(0) end)}
            )

            GenServer.cast(__MODULE__, {:successful_call})

          _ ->
            GenServer.reply(respond_to, :error)
            GenServer.cast(__MODULE__, {:errored_call})
        end
    end
  end

  def handle_call(
        {:lookup, prefix_hash},
        from,
        {next_appropriate_call_ms, error_count}
      ) do
    # Preliminary checks first. Get the current system time
    current_time = :os.system_time(:milli_seconds)
    # And the time difference between
    time_diff = next_appropriate_call_ms - current_time

    # Check how long we are willing to wait
    max_wait_ms = Application.get_env(:ikuturso, :hibp_maximum_wait_time, 500)

    cond do
      # We would have to wait too long, return a timeout
      time_diff > max_wait_ms ->
        {:reply, :try_again_later, {next_appropriate_call_ms, error_count}}

      true ->
        # Wait a bit if needed
        if time_diff > 50 do
          Process.sleep(time_diff)
        end

        # Launch an asynchronous task
        Task.start(IkuTurso.ExternalConnections.HIBP, :async_hibp_request, [from, prefix_hash])

        # Return without reply
        {:noreply, {next_appropriate_call_ms, error_count}}
    end
  end
end
