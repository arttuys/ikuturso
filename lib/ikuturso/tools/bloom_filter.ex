defmodule IkuTurso.Tools.BloomFilter do
  use Bitwise

  @moduledoc """
  BloomFilter, as it is named, is a specialized bloom filter module for the purposes of IkuTurso.
  Its express purpose is to efficiently store a vast amount of password hashes, allowing rapid checking for if a given password is compromised.

  The purpose of this module is to execute following tasks:
  - Load suitably formatted bloom filter files (more about format later)
  - Generate such filters from strings
  - Answer inquiries, with a given probability, if a certain string has been inserted to the filter

  For efficiency purposes when working, Erlang fixed-size arrays are used as storage.

  ## Data format

  Bloom filters are stored in a simple data format:
  - First 8-bit byte is the hash count
  - Rest of the data found (length implicitly measured as a count before EOF) is treated as the raw bloom filter data, as indexed by bytes first to last, LSB to MSB inside a byte
  """

  defstruct bytes: nil, hash_count: 0

  @doc """
  Returns a tuple describing the ideal properties (smallest possible size and hash count) of a bloom filter, given a certain item count one wishes to store and tolerated error probability.
  Properties are returned in a tuple, `{number_of_bits, number_of_hashes}`
  """
  def ideal_properties(desired_items_count, err_probability)
      when is_integer(desired_items_count) and is_number(err_probability) and
             desired_items_count > 0 and err_probability > 0 and err_probability < 1 do
    # Depending on the characteristics we desire, we need to vary the size of our bloom filter
    # Per http://people.math.gatech.edu/~randall/AlgsF09/bloomfilters.pdf, we can calculate the appropriate bit count for a given false positive rate and size
    num_bits =
      Float.ceil(desired_items_count * (:math.log2(1.0 / err_probability) / :math.log(2)))
      |> Kernel.trunc()

    # There is an equation for approximating the proper count of hash functions, also given by the proof above
    num_hashes =
      max(
        1,
        Float.floor(num_bits / desired_items_count * :math.log(2))
        |> Kernel.trunc()
      )

    {bytes_needed(num_bits) * 8, num_hashes}
  end

  def ideal_properties(_, _), do: raise("invalid arguments given")

  defp bytes_needed(bits) do
    # Always round up to the nearest full byte
    div(bits, 8) + 1
  end

  defp calculate_hashes_for_term_internal(_, 0) do
    []
  end

  defp calculate_hashes_for_term_internal(item, hash_count) do
    # Due to a need to calculate multiple hashes, we simply stack them
    # max_range signifies the upper bound exclusive, hash_count the count of hashes we need
    data_hash = :erlang.phash2(item, 4_294_967_296)

    # Tail call recursively
    [data_hash | calculate_hashes_for_term_internal(data_hash, hash_count - 1)]
  end

  defp calculate_hashes_for_term(item, max_range, hash_count) do
    calculate_hashes_for_term_internal(item, hash_count)
    |> Enum.map(fn hash -> Integer.mod(hash, max_range) end)
  end

  defp hash_to_index_pair(hash) do
    # Return an index pair: the appropriate byte to visit, and the proper AND/OR value for checking/assigning a hash
    remainder = rem(hash, 8)

    {
      ((hash - remainder) / 8)
      |> round,
      :math.pow(2, remainder)
      |> round
    }
  end

  # Public functionality

  @doc """
  Constructs an empty bloom filter, specifically optimized for a specified maximum item count and false positive rate for the given maximum of items.

  This function will raise if an excessively sized or a very low-false-positive bloom filter is attempted. This does not arise in most normal use-cases, but is something to keep in mind.
  """
  def empty_bloom_filter(desired_items_count, err_probability)
      when is_integer(desired_items_count) and is_number(err_probability) and
             desired_items_count > 0 and err_probability > 0 and err_probability < 1 do
    {bits_required, hash_count} = ideal_properties(desired_items_count, err_probability)

    raw_bloom_filter(bits_required, hash_count)
  end

  def empty_bloom_filter(_, _), do: raise("invalid properties for a bloom filter")

  @doc """
  Constructs a raw bloom filter. This assumes you have knowledge of appropriate raw parameters for your use-case.
  Do note that the returned bloom filter has the bit count provided rounded down to nearest multiple of 8
  """
  def raw_bloom_filter(bits_required, hash_count)
      when is_integer(bits_required) and is_integer(hash_count) and bits_required > 0 do
    # Return an empty structure

    if bits_required > 4_294_967_296 do
      raise "cannot create a larger range of bits than 2^32"
    end

    if hash_count > 255 || hash_count < 1 do
      raise "cannot create more hashes than 255 or less than 1"
    end

    %IkuTurso.Tools.BloomFilter{
      bytes: :array.new(size: div(bits_required, 8), fixed: true, default: 0),
      hash_count: hash_count
    }
  end

  def raw_bloom_filter(_, _) do
    raise "invalid arguments given for a raw bloom filter"
  end

  @doc """
  Mark an item as a part of this bloom filter. After this call, it is guaranteed that the provided item can be found using `check`
  However, as expected with bloom filters, there may be false positives when items are added. This can be particularly pronounced with suboptimal parameters (e.g too few bytes, far too many hashes)
  """
  def mark(structure, item) do
    # Generate hashes
    hashes =
      calculate_hashes_for_term(item, :array.size(structure.bytes) * 8, structure.hash_count)
      |> Enum.map(fn x -> hash_to_index_pair(x) end)

    # Return new structure
    %{
      structure
      | bytes:
          Enum.reduce(
            hashes,
            structure.bytes,
            fn {index, or_num}, acc ->
              :array.set(index, bor(or_num, :array.get(index, acc)), acc)
            end
          )
    }
  end

  @doc """
  Check if a given item is a part of the bloom filter. This is guaranteed to return `true` for any object that marked using `mark`, but
  may also return `true` as a false positive according to the characteristics of the bloom filter.
  """
  def check(structure, item) do
    # Generate hashes
    hashes =
      calculate_hashes_for_term(item, :array.size(structure.bytes) * 8, structure.hash_count)
      |> Enum.map(fn x -> hash_to_index_pair(x) end)

    # Check that all hashes are found
    Enum.all?(hashes, fn {index, and_num} ->
      band(:array.get(index, structure.bytes), and_num) == and_num
    end)
  end

  @doc """
  Returns a binary representation of this bloom filter. The first byte will be the hash count, while the rest will be the bytes of this bloom filter
  """
  def to_binary(structure) do
    <<
      structure.hash_count::size(8),
      :array.to_list(structure.bytes)
      |> :binary.list_to_bin()::binary
    >>
  end

  @doc """
  Converts a binary representation to a bloom filter. Will raise if some primitive sanity checks fail, but caller should still ensure that the input data is valid and verified
  """
  def from_binary(<<hash_count::size(8), data::binary>>) do
    if hash_count == 0, do: raise("invalid binary, hash count zero")
    bs = byte_size(data)

    if bs == 0 || bs * 8 > 4_294_967_296,
      do: raise("invalid binary, no or excessive amount of data")

    %IkuTurso.Tools.BloomFilter{
      hash_count: hash_count,
      bytes:
        :binary.bin_to_list(data)
        |> :array.from_list(0)
        |> :array.fix()
    }
  end

  @doc """
  Checks if a certain item is found from a binary. This is most useful for scenarios where one wishes to avoid creating an extra structure, when it is known that no further editing will be needed.
  """
  def check_from_binary(<<hash_plus_data::binary>>, item) do
    hash_count = :binary.at(hash_plus_data, 0)
    bs = byte_size(hash_plus_data) - 1

    if hash_count == 0, do: raise("invalid binary, hash count zero")

    if bs == 0 || bs * 8 > 4_294_967_296,
      do: raise("invalid binary, no or excessive amount of data")

    hashes =
      calculate_hashes_for_term(item, bs * 8, hash_count)
      |> Enum.map(fn x -> hash_to_index_pair(x) end)

    Enum.all?(hashes, fn {index, and_num} ->
      band(:binary.at(hash_plus_data, index + 1), and_num) == and_num
    end)
  end
end
