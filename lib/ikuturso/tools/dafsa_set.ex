defmodule IkuTurso.Tools.DAFSASet do
  use Bitwise

  @moduledoc """
  This module implements a fairly naive DAFSA (deterministic acyclic finite state automaton), designed to rapidly locate its contents as substrings of a given string, and store auxillary numeric data along with these words.
  It is designed for the express purpose of allowing rapid determination of word components of a given password, giving a way to approximate how quickly those components could be guessed.

  DAFSAs are saved to and loaded from Zlib-compressed JSON files, decoded and encoded with Poison to native data structures
  """
  defstruct map: nil

  @doc """
  Builds a DAFSA set. These sets are unalterable after creation; all words must be ingested in one go

  It should be ensured that strings do not contain NUL (byte value `0`) characters; any NUL characters are treated as an end of string, and can result in apparent misbehavior
  """
  def build_dafsa_set(binary_list, auto_order \\ true)
      when is_list(binary_list) and length(binary_list) > 0 and is_boolean(auto_order) do
    # First, construct tuples with orders, if required
    tupled_list =
      if auto_order,
        do:
          binary_list
          |> Stream.with_index(),
        else: binary_list

    # For avoidance of ambiguity (one byte with a match to multiple possible jumps), we use a zero byte as an EOW step. This adds a small space penalty, but simplifies code as steps are unambiguous
    {final_structure, _last_unused_state_id} =
      Enum.reduce(
        tupled_list,
        {%{}, 1},
        fn {str, order}, acc -> walk_and_insert_binary_to_map(acc, 0, {str <> <<0>>, order}) end
      )

    # Return final structure
    %IkuTurso.Tools.DAFSASet{map: final_structure}
  end

  @doc """
  Generates a binary describing this DAFSA. This binary can be used later to reload the same DAFSA without regeneration being required
  """
  def to_compressed_data(%IkuTurso.Tools.DAFSASet{map: map}) do
    # Convert the map format to a convenient string representation
    Poison.encode!(
      Enum.reduce(
        map,
        %{},
        fn {state, data}, acc ->
          inner_content =
            Enum.reduce(
              data,
              %{},
              fn {byte, repr}, inner_acc ->
                Map.put(
                  inner_acc,
                  Integer.to_string(byte),
                  case repr do
                    {:score, score} -> ["score", score]
                    {:jump, next} -> ["jump", next]
                  end
                )
              end
            )

          Map.put(acc, Integer.to_string(state), inner_content)
        end
      )
    )
    |> IO.iodata_to_binary()
    |> :zlib.compress()
  end

  @doc """
  Loads a DAFSA from a compressed binary as generated prior by `to_compressed_data`

  This function will throw if the binary is corrupted or otherwise unloadable.
  """
  def from_compressed_data(compressed_json) do
    # Convert map back to ordinary form
    %IkuTurso.Tools.DAFSASet{
      map:
        :zlib.uncompress(compressed_json)
        |> Poison.decode!()
        |> Enum.reduce(
          %{},
          fn {state, data}, acc ->
            inner_content =
              Enum.reduce(
                data,
                %{},
                fn {byte, repr}, inner_acc ->
                  Map.put(
                    inner_acc,
                    String.to_integer(byte),
                    case repr do
                      ["score", score] -> {:score, score}
                      ["jump", next] -> {:jump, next}
                    end
                  )
                end
              )

            Map.put(acc, String.to_integer(state), inner_content)
          end
        )
    }
  end

  @doc """
    Finds substrings from a DAFSA set. Returns an array of tuples of form `{substring, position, score}`

    By default, substrings can picked from any point in the string, and wildcards are not enabled (wildcard symbols are treated as any other byte). Parameters `require_first_letter_match` and `enable_wildcards` change this.

    ## Wildcards

    - `?` matches any single byte
    - `*` matches a single or multiple bytes after the first matched byte. No further bytes will be checked from the pattern after a `*`, so currently this can only be used for prefix search
  """
  def find_substrings(
        %IkuTurso.Tools.DAFSASet{map: map},
        str,
        require_first_letter_match \\ false,
        allow_empty_matches \\ false,
        enable_wildcards \\ false
      ) do
    # First, generate substrings if necessary
    str_len = String.length(str)
    range = 0..(str_len - 1)

    # Enumerate possible substring matches. If we allow matching from any point, generate substrings for doing that
    indexed_strings =
      if require_first_letter_match,
        do: [{str, 0}],
        else: Enum.map(range, fn x -> {String.slice(str, x, str_len), x} end)

    Enum.map(
      indexed_strings,
      fn {subpattern, pos} ->
        walk_and_find_substring_matches(map, 0, subpattern, <<>>, {enable_wildcards, false})
        |> List.flatten()
        |> Enum.map(fn {f_str, score} -> {f_str, pos, score} end)
      end
    )
    |> List.flatten()
    |> Enum.filter(fn x -> allow_empty_matches || elem(x, 0) != "" end)
  end

  # Implicitly skip zero bytes as found from the binary; these are used as internal EOW indicators, and should not be found from the pattern itself
  defp walk_and_find_substring_matches(
         map,
         state_pointer,
         <<0::size(8), rest::binary>>,
         found,
         properties
       ),
       do: walk_and_find_substring_matches(map, state_pointer, rest, found, properties)

  defp walk_and_find_substring_matches(
         map,
         state_pointer,
         remaining,
         found,
         {allow_wildcard, star_found_before}
       ) do
    # First, define what we will allow for matches
    {valid_jumps, has_star} =
      case remaining do
        <<"*", _::binary>> ->
          if allow_wildcard, do: {[], true}, else: {[0, "*"], false}

        <<"?", _::binary>> ->
          if allow_wildcard,
            do: {
              0..255
              |> Enum.to_list(),
              false
            },
            else: {[0, "?"], false}

        <<byte::size(8), _::binary>> ->
          {[0, byte], false}

        _ ->
          {[0], false}
      end

    star_found = star_found_before || has_star

    # Select appropriate state from the map - or in the odd case that we do not have any, return nothing
    available_paths_now = Map.get(map, state_pointer, %{})

    # Select appropriate jumps, and merge appropriately
    Enum.filter(
      if(star_found, do: 0..255, else: valid_jumps),
      fn jump_b -> Enum.member?(Map.keys(available_paths_now), jump_b) end
    )
    |> Enum.map(fn jump_b ->
      # Check what applicable path we have
      case Map.get(available_paths_now, jump_b) do
        {:score, score} ->
          [{found, score}]

        # We need to recurse. Let's calculate the next state
        {:jump, state} ->
          # Allow popping from remaining bytes if we don't have a zero/EOW or out of bytes
          can_pop_from_remaining = byte_size(remaining) > 0 && jump_b != 0
          # We cannot push if we have a zero/EOW, otherwise we can
          can_push_to_found = jump_b != 0

          next_remaining =
            if can_pop_from_remaining,
              do: :binary.part(remaining, 1, byte_size(remaining) - 1),
              else: remaining

          next_found = if can_push_to_found, do: found <> <<jump_b>>, else: found

          walk_and_find_substring_matches(
            map,
            state,
            next_remaining,
            next_found,
            {allow_wildcard, star_found}
          )
      end
    end)
  end

  defp walk_and_insert_binary_to_map(
         {map, next_free_index},
         state_pointer,
         {<<byte::size(8), rest::binary>>, actual_score}
       ) do
    # We need to keep track of the map, an index counter to know the next free index for a new state, appropriate score data, and the actual string we wish to match to

    # First, determine what paths we have now from our current state - or inject an empty default otherwise
    available_paths_now = Map.get(map, state_pointer, %{})

    # Then, determine if our byte already leads to somewhere
    found_transition = Map.get(available_paths_now, byte)

    case found_transition do
      {:jump, next_state} ->
        # We already have a route to an another state. We do not need to change anything here, recurse forward
        walk_and_insert_binary_to_map({map, next_free_index}, next_state, {rest, actual_score})

      {:score, _} ->
        # Our step is an EOW, which means this is a duplicate addition. Do not change anything, return our map as is
        {map, next_free_index}

      _ ->
        # We do not have a valid jump. However, we need to check if this is the last byte we match, This is enforced by either checking if we are running out of bytes, or that we are adding a path from a zero byte, which is treated as a special EOW indicator
        eow = rest == <<>> || byte == 0
        transition = if eow, do: {:score, actual_score}, else: {:jump, next_free_index}

        # Save this to our map
        new_map =
          Map.put(
            map,
            state_pointer,
            available_paths_now
            |> Map.put(byte, transition)
          )

        # If we did just EOW, return as is, otherwise recurse
        if eow do
          {new_map, next_free_index}
        else
          walk_and_insert_binary_to_map(
            {new_map, next_free_index + 1},
            next_free_index,
            {rest, actual_score}
          )
        end
    end
  end

  @doc """
  Returns highest possible score that can be found from this map.
"""
  def maximum_score(dafsa, initial_state \\ 0) do
    %IkuTurso.Tools.DAFSASet{map: map} = dafsa
    available_paths_now = Map.get(map, initial_state, %{})

    # Reduce to determine appropriate path
    Enum.reduce(available_paths_now, 0, fn {_, next_hop}, tally ->
      case next_hop do
        {:score, score} -> max(tally, score)
         {:jump, next_state} -> max(tally, maximum_score(dafsa, next_state))
      end
    end)
  end
end
