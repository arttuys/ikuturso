defmodule IkuTurso.Tools.UnicodeClasses do
  # Due to the nature of the functionality, this module is defined in a slightly atypical fashion
  # First, load Unicode block data as a stream
  unicode_block_stream = File.stream!(Path.join(
    :code.priv_dir(:ikuturso),
    Path.join(["raw", "unicode", "Blocks.txt"])
  ))

  # Declare a regex for this
  block_regex = ~r/\A(?<begin>[0-9A-F]+)\.\.(?<end>[0-9A-F]+); (?<name>.+)\z/i

  # Declare destructuring function
  def block_name(<<x :: utf8>>), do: block_name(x)

  # Process it, temporarily saving
  codepoint_to_block_map = Enum.reduce(unicode_block_stream, %{}, fn line, map ->
    data = Regex.named_captures(block_regex, line |> String.trim())

    if data != nil do
      # This is a valid line. Declare range as such
      range = (data["begin"] |> String.to_integer(16))..(data["end"] |> String.to_integer(16))
      name = data["name"]

      # Declare a function clause for the appropriate block name
      def block_name(codepoint) when codepoint in unquote(Macro.escape(range)), do: unquote(name)
      # And declare the size of the range
      def range_size(unquote(name)), do: unquote(Enum.count(range))

      # Save all to range, and return filled map
      Enum.reduce(range, map, fn val, map -> Map.put(map, val, name) end)
    else
      map
    end
  end)

  # Declare a default option for both
  def block_name(_), do: nil
  def range_size(_), do: nil

  ######
  # Next, load category data
  unicode_case_stream = File.stream!(Path.join(
    :code.priv_dir(:ikuturso),
    Path.join(["raw", "unicode", "DerivedGeneralCategory.txt"])
  ))

  # Declare a regex for DGC data
  dgc_regex = ~r/\A(?<begin>[0-9A-F]+)(\.\.(?<end>[0-9A-F]+))? *; (?<catg>.+) #.*\z/i
  # Build two maps: combined count map (how many codepoints exist for a given pair of a block and a general category) and a codepoint to category map (in this case, inverse: category leads to codepoint)
  {combined_count_map, catg_to_codepoint_map} = Enum.reduce(unicode_case_stream, {%{}, %{}}, fn line, acc ->
   data = Regex.named_captures(dgc_regex, line |> String.trim())

    if data != nil do
      range = (data["begin"] |> String.to_integer(16))..((if data["end"] == "", do: data["begin"], else: data["end"]) |> String.to_integer(16))
      catg = data["catg"]

      Enum.reduce(range, acc, fn codepoint, {combined_map, codepoint_to_catg_map} ->
        # First, update combined map
        combined_map_key = {Map.get(codepoint_to_block_map, codepoint, "Undefined"), catg}
        # Get previous value
        combined_map_prev_val = Map.get(combined_map, combined_map_key, 0)
        # Add by one
        new_combined_map = Map.put(combined_map, combined_map_key, combined_map_prev_val + 1)

        # Then, update codepoint to category map
        ctcatg_prev_val = Map.get(codepoint_to_catg_map, catg, [])
        # Update map with a new codepoint
        new_ctcatg_map = Map.put(codepoint_to_catg_map, catg, [codepoint | ctcatg_prev_val])

        {new_combined_map, new_ctcatg_map}
      end)
    else
     acc
    end
  end)

  def general_category(<<x :: utf8>>), do: general_category(x)

  # Declare appropriate functions for these maps
  Enum.each(Map.keys(catg_to_codepoint_map), fn key ->
    def codepoints_for_category(unquote(key)), do: unquote(Map.get(catg_to_codepoint_map, key))
  end)

  Enum.each(combined_count_map, fn {key, count} ->
    def block_catg_pair_count(unquote(key)), do: unquote(count)
  end)

  # Define a key listing for the functions above
  def block_catg_pair_keys(), do: unquote(Map.keys(combined_count_map))

  # Due to the problems related to excessively sized guards, we slightly bypass this by creating a find function
  # that dynamically goes through valid categories and checks each
  def general_category(val) do
    valid_categories = unquote(Map.keys(catg_to_codepoint_map))

    Enum.find(valid_categories, nil, fn catg -> val in codepoints_for_category(catg) end)
  end

  def codepoints_for_category(_), do: nil
  def block_catg_pair_count(_), do: nil

end