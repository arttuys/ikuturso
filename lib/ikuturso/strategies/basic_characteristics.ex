defmodule IkuTurso.Strategies.BasicCharacteristics do
  @character_types [["Lu", "Lt", "Lm", "Lo"], ["Ll", "Lo"], ["Nd", "Nl", "No"], ["Pc", "Pd","Ps", "Pe", "Pf", "Po", "Sm", "Sc", "Sk", "So", "Zs", "Mn", "Mc", "Me"]]

  @moduledoc """
  **BasicCharacteristics** strategy measures certain baseline characteristics of a password.

  This alone is not sufficient to ensure a fair level of password complexity, it is intended to be more of a preliminary filter to block patently bad passwords from more expensive tests.

  This strategy can check:
    - Password is of appropriate length
    - Password contains enough categories of characters from following:
      - Lowercase letters
      - Uppercase letters
      - Numeric characters
      - Symbols and combining marks
  """
  @behaviour IkuTurso.Strategies.Strategy
  defp check_characteristic({:max_length, length}, str) do
    cond do
      String.length(str) > length -> {:conflict, {:too_long, length}}
      true -> {:success}
    end
  end

  defp check_characteristic({:min_length, length}, str) do
    cond do
      String.length(str) < length -> {:conflict, {:too_short, length}}
      true -> {:success}
    end
  end

  defp check_characteristic({:req_catg, req_min}, str) do
    # Enumerate through categories, and stop once we have exhausted all categories we need
    remaining_catgs = Enum.reduce_while(String.codepoints(str), @character_types, fn codepoint, remaining_sets ->
      char_class = IkuTurso.Tools.UnicodeClasses.general_category(codepoint)
      next_remaining = Enum.reject(remaining_sets, fn set -> (char_class in set) end)

      if length(next_remaining) <= (length(@character_types) - req_min) do
        {:halt, nil}
      else
        {:cont, next_remaining}
      end
    end)

    if remaining_catgs == nil, do: {:success}, else: {:conflict, {:insufficient_catgs, req_min}}
  end

  defp check_characteristic(_, _) do
    {:error}
  end

  @impl IkuTurso.Strategies.Strategy

  def run_strategy(%IkuTurso.Strategies.Strategy{module: _, aux_data: characteristics}, str) do
    characteristics
    |> Enum.to_list()
    |> Enum.map(fn characteristic -> check_characteristic(characteristic, str) end)
    |> Enum.min_by(&IkuTurso.Strategies.Helpers.sort_value_function/1, fn -> {:success} end)
  end

  def generate_strategy(min_pwd_length \\ 8, max_pwd_length \\ 128, require_categories \\ 3)
      when is_integer(min_pwd_length) and is_integer(max_pwd_length) and is_integer(require_categories) do
    strategy_metadata =
      [
        if(min_pwd_length < 1, do: nil, else: {:min_length, min_pwd_length}),
        if(max_pwd_length < 1, do: nil, else: {:max_length, max_pwd_length}),
        if(require_categories < 1, do: nil, else: {:req_catg, require_categories})
      ]
      |> Enum.filter(fn x -> x != nil end)
      |> Enum.to_list()

    %IkuTurso.Strategies.Strategy{
      module: IkuTurso.Strategies.BasicCharacteristics,
      aux_data: strategy_metadata
    }
  end
end
