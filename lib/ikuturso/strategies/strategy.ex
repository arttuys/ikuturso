defmodule IkuTurso.Strategies.Strategy do
  defstruct module: nil, aux_data: nil

  @type strategy_specification :: IkuTurso.Strategies.Strategy

  @moduledoc """
    ## Types for returns

    IkuTurso strategies can return 4 different types of messages. Each behaves differently, as explained below.
    Apart from `:success` and `:error` codes, strategies are expected to return strategy-specific error atoms (possibly lists of such atoms combined with other values) as a part of their return value.

    - `:error` signifies that something went wrong with the check. This is meant to signify an internal error, that ideally should have no relation to the password itself
    - `:conflict` signifies a _conflict_. A conflict is equivalent to a critical, show-stopping error - you return a conflict if the given password does clearly _not_ fit the qualifications of the testing strategy.
    - `:warning` signifies a _non-critical warning_. A warning will not stop processing or fail anything, but should be shown to an user. An appropriate moment to show a warning would be, for example, if the user's password qualifies by a very thin margin - qualifies, but any non-optimal change could render it non-conforming.
    - `:success` signifies that the test passed, without warnings. An ideal password should have all tests return `:success`

    Most severe error code will apply for a given primary message specifier (first and possibly only atom) per cycle, in the order above from top to bottom. In case of multiple warnings, latest one issued is returned.
    It is not an error to have multiple return types with the same specifier
  """
  @type message_specifier :: atom() | {atom(), any}
  @type return_data ::
          {:success}
          | {:error}
          | {:warning, message_specifier}
          | {:conflict, message_specifier}

  @callback run_strategy(strategy :: strategy_specification, str :: String.t()) :: return_data
end
