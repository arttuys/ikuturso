defmodule IkuTurso.Strategies.Helpers do
  def sort_value_function({:success}), do: 4
  def sort_value_function({:warning, _}), do: 3
  def sort_value_function({:conflict, _}), do: 2
  def sort_value_function({:error}), do: 1
  def sort_value_function(_), do: raise("unsortable helper value")
end
