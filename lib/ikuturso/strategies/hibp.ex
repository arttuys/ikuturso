defmodule IkuTurso.Strategies.HIBP do
  @moduledoc """
    **HIBP** strategy uses the local HIBP dictionary and/or online HIBP API to check if a password is compromised

    Do note that this strategy should be reused when instantiated; the local dictionary is loaded on instantiation, and constant recreation will result in suboptimal performance.
  """
  @behaviour IkuTurso.Strategies.Strategy
  @impl IkuTurso.Strategies.Strategy

  def run_strategy(
        %IkuTurso.Strategies.Strategy{
          module: _,
          aux_data: {local_dict_binary, ask_online, strict_online_errors}
        },
        str
      ) do
    if local_dict_binary == nil ||
         !IkuTurso.Tools.BloomFilter.check_from_binary(local_dict_binary, str) do
      if !ask_online do
        # No online checks required
        {:success}
      else
        result = IkuTurso.ExternalConnections.HIBP.check_password_compromise(str)

        case result do
          :ok -> {:success}
          :fail -> {:conflict, :known_compromised}
          _ -> {if(strict_online_errors, do: :conflict, else: :warning), :hibp_failed}
        end
      end
    else
      # Found from local dictionary, compromised
      {:conflict, :known_compromised}
    end
  end

  @doc """
    Generates a HIBP strategy. You can change following options:
      - Should one use a local directory
      - Should one check online
      - Treat online check errors as warnings or conflicts
  """
  def generate_strategy(
        local_directory \\ true,
        online_directory \\ true,
        strict_online_errors \\ false
      )
      when is_boolean(local_directory) and is_boolean(online_directory) and
             is_boolean(strict_online_errors) do
    %IkuTurso.Strategies.Strategy{
      module: IkuTurso.Strategies.HIBP,
      aux_data: {
        if(local_directory,
          do:
            File.read!(
              Path.join(
                :code.priv_dir(:ikuturso),
                Path.join(["datasets", "hibp-top-1-percent.iktb"])
              )
            ),
          else: nil
        ),
        online_directory,
        strict_online_errors
      }
    }
  end
end
